﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace USBCQuestion1.Tests
{
    [TestClass]
    public class USBCQuestion1_Tests
    {
        /// <summary>
        /// Get a string writer for the console output
        /// </summary>
        /// <returns></returns>
        protected StringWriter Console()
        {
            var writer = new StringWriter();
            System.Console.SetOut(writer);
            return writer;
        }

        /// <summary>
        /// Verify that count logger properly prefixes with count
        /// </summary>
        [TestMethod]
        public void Logger_CountLogger()
        {
            using (var console = Console())
            {
                var logger = new CountLogger();
                logger.Log("First");
                logger.Log("Second");
                logger.Log("Third");
                logger.Log("Fourth");
                Assert.IsTrue(logger.Count == 4);

                var output = console.ToString();
                Assert.IsTrue(output.Contains("1: First"));
                Assert.IsTrue(output.Contains("2: Second"));
                Assert.IsTrue(output.Contains("3: Third"));
                Assert.IsTrue(output.Contains("4: Fourth"));
            }
        }

        /// <summary>
        /// Verify that date logger properly prefixes logs with date
        /// </summary>
        [TestMethod]
        public void Logger_DateLogger()
        {
            using (var console = Console())
            {
                var logger = new DateLogger();

                var firstTime = DateTime.Now;
                logger.Log("First");

                var secondTime = DateTime.Now;
                logger.Log("Second");
                
                var thirdTime = DateTime.Now;
                logger.Log("Third");

                var fourthTime = DateTime.Now;
                logger.Log("Fourth");

                var output = console.ToString();
                Assert.IsTrue(output.Contains($"{firstTime:G}: First"));
                Assert.IsTrue(output.Contains($"{secondTime:G}: Second"));
                Assert.IsTrue(output.Contains($"{thirdTime:G}: Third"));
                Assert.IsTrue(output.Contains($"{fourthTime:G}: Fourth"));
            }
        }

        /// <summary>
        /// Verify that program arguments work properly
        /// </summary>
        [TestMethod]
        public void Program_Arguments()
        {

            // empty args, defaults to date
            using (var console = Console())
            {
                Program.Main(new string[] { });
                var output = console.ToString();
                Assert.IsTrue(output.Contains("Using Date Logger"));
            }

            // Invalid Logger, Outputs Error
            using (var console = Console())
            {
                var loggerName = "UnknownLogger";
                Program.Main(new string[] { loggerName });
                var output = console.ToString();
                Assert.IsTrue(output.Contains($"Invalid logger: {loggerName}"));
            }
            
            // Date Logger
            using (var console = Console())
            {
                Program.Main(new string[] { "Date" });
                var output = console.ToString();
                Assert.IsTrue(output.Contains("Using Date Logger"));
            }
            
            // Count Logger
            using (var console = Console())
            {
                Program.Main(new string[] { "Count" });
                var output = console.ToString();
                Assert.IsTrue(output.Contains("Using Count Logger"));
            }
        }
    }
}
