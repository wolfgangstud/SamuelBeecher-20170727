﻿namespace USBCQuestion1
{
    /// <summary>
    /// Class for administration functions
    /// </summary>
    public class Admin
    {
        protected readonly ILogger Logger;

        public Admin(ILogger logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Run the application
        /// </summary>
        public void Run()
        {
            Logger.Log("Starting Application");
            Logger.Log("Running Application");
            Logger.Log("Ending Application");
            Logger.Log("Complete");
        }
    }
}