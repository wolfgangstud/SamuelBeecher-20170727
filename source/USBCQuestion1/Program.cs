﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USBCQuestion1
{
    public class Program
    {
        public const string HelpText = @"USBCQuestion1.exe {Logger:optional}
    Logger Options:
        Date  - Logs include the Date stamp (Default)
        Count - Logs include the log #
    ";
        public const string DateLogger = "Date";
        public const string CountLogger = "Count";

        public static void Main(string[] args)
        {
            var loggerName = DateLogger;
            if (args.Any())
            {
                loggerName = args[0].Trim();
            }
            
            // Output the help text for reference
            Console.WriteLine(HelpText);
            
            // Get the logger as specified
            ILogger logger = null;
            if (loggerName.Equals(DateLogger, StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Using Date Logger");
                logger = new DateLogger();
            }
            else if(loggerName.Equals(CountLogger, StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Using Count Logger");
                logger = new CountLogger();
            }

            // If the logger was successfully initialized, run the program
            if (logger != null)
            {
                var admin = new Admin(logger);
                admin.Run();
            }
            else // Otherwise communicate the error.
            {
                Console.WriteLine($"Invalid logger: {loggerName}");
            }
            
        }
    }
}
