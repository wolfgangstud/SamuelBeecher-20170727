﻿namespace USBCQuestion1
{
    /// <summary>
    /// Interface for logging messages
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Log the message to the console
        /// </summary>
        void Log(string message);
    }
}