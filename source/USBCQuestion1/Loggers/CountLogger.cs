﻿using System;

namespace USBCQuestion1
{
    /// <summary>
    /// Log a message with the count
    /// </summary>
    public class CountLogger : ILogger
    {
        public int Count { get; protected set; }

        public void Log(string message)
        {
            Count++;
            Console.WriteLine($"{Count}: {message}");
        }
    }
}