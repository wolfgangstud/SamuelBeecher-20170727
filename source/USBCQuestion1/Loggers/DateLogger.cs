﻿using System;

namespace USBCQuestion1
{
    /// <summary>
    /// Log each message with the date included
    /// </summary>
    public class DateLogger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine($"{DateTime.Now:G}: {message}");
        }
    }
}