# USBC Question 1


## To Run
In command prompt, go to Release folder and run "USBCQuestion1.exe"

Example:  
```USBCQuestion1.exe Count```

## To Compile
1. Open .sln in Source folder 
2. Rebuild 
3. .exe and associated files are automatically copied to "release" folder in project root
